%% WARNING: This class requires babel (for \addto\captions...), but it's not loaded here to avoid messing up with its options and the package loading order.

%%%% Identification

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{labreport}[2017/02/15 v0.3]

%%%% Initial code

\newif\if@footers
\@footersfalse

%%%% Declaration of options

\DeclareOption{skip}
{
  \AtEndOfClass
  {
    \setlength{\parindent}{0em}
    \setlength{\parskip}{\medskipamount}
  }
}
\DeclareOption{indent}
{
  \AtEndOfClass
  {
    \setlength{\parindent}{1em}
    \setlength{\parskip}{0pt}
  }
}
\DeclareOption{footers}
{
  \@footerstrue
}
\ExecuteOptions{footers,indent}
\ProcessOptions\relax

%%%% Package loading

\LoadClassWithOptions{article}
\RequirePackage{etoolbox} % For \patchcmd and \ifundef
\RequirePackage{xpatch} % For \xpretocmd
\RequirePackage[margin=1in,heightrounded]{geometry}
\@ifclasswith{article}{twoside}
  {\geometry{bindingoffset=.5in}} % This makes the marginnotes go beyond the border of the page. I didn't take care of this since marginnotes aren't usually needed in lab reports. If you need them, you should adjust the \marginparwidth with something like '\setlength{\marginparwidth}{\dimexpr \marginparwidth - 2\Gm@bindingoffset \relax}' (the "2" that multiplies '\Gm@bindingoffset' is arbitrary, see ltxguide).
  {\geometry{bindingoffset=0in}}
\if@footers
  \RequirePackage{fancyhdr} % fancyhdr must be loaded after geometry is completely set up.
\fi

%%%% Main code

\frenchspacing
\setcounter{tocdepth}{1}
\renewcommand{\thepart}{\arabic{part}}
\ifundef{\abstract}
{}
{
  \patchcmd{\abstract} % Remove the intentation from the first line of the abstract. tex.stackexchange.com/questions/1597/remove-paragraph-indent-from-abstract-in-article-class/1603#1603
    {\quotation}
    {\quotation\noindent\ignorespaces}
    {}{}
}
\AtBeginDocument
{
  \numberwithin{section}{part} % It conflicts with hyperref and biblatex if it's not delayed until \begin{document}
  \numberwithin{equation}{part}
  \numberwithin{table}{part}
  \numberwithin{figure}{part}
  \addto\captionsitalian{\renewcommand{\partname}{Esperienza}}
  \addto\captionsenglish{\renewcommand{\partname}{Experiment}}
  \addto\captionsitalian{\renewcommand{\abstractname}{\textmd{Sommario}}} % Redefine to {\vspace{-\baselineskip}} to remove the abstract heading. tex.stackexchange.com/questions/42482/remove-abstract-title-from-abstract/53175#53175
  \addto\captionsenglish{\renewcommand{\abstractname}{\textmd{Abstract}}} % Redefine to {\vspace{-\baselineskip}} to remove the abstract heading. tex.stackexchange.com/questions/42482/remove-abstract-title-from-abstract/53175#53175  
}
\if@footers
  \pagestyle{fancy}
  \renewcommand{\headrulewidth}{0pt}
  \fancyhf{}
  \@ifclasswith{article}{twoside}
  {
    \fancyfoot[EL,OR]{\thepage}
    \fancyfoot[ER,OL]{\partname\ \thepart}
    \fancypagestyle{plain}
    {%
      \fancyhf{}%
      \fancyfoot[EL,OR]{\thepage}%
    }
  }
  {
    \fancyfoot[C]{\thepage \quad $\cdot$\quad \partname\ \thepart}
    \fancypagestyle{plain}
    {%
      \fancyhf{}%
      \fancyfoot[C]{\thepage}%
    }
  }
  \AtBeginDocument % or it doesn't work
  {
    \xpretocmd{\@part}{\thispagestyle{plain}}{}{} % \xpretocmd deals better with commands with optional arguments or declared robust, compared with \pretocmd. https://tex.stackexchange.com/questions/152773/please-tutor-the-usage-of-patchcmd-and-xpatch
  }
\fi
