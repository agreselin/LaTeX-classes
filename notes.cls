%%%% Identification

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{notes}[2017/02/15 v0.3]

%%%% Declaration of options

\DeclareOption{skip}
{
  \AtEndOfClass
  {
    \setlength{\parindent}{0em}
    \setlength{\parskip}{\medskipamount}
  }
}
\DeclareOption{indent}
{
  \AtEndOfClass
  {
    \setlength{\parindent}{1em}
    \setlength{\parskip}{0pt}
  }
}
\ExecuteOptions{skip}
\ProcessOptions\relax

%%%% Package loading

\LoadClassWithOptions{article}
\RequirePackage{mathtools} % For the option fleqn and the command \numberwithin

%%%% Main code

\frenchspacing
\setcounter{tocdepth}{2}
\renewcommand{\thepart}{\arabic{part}}